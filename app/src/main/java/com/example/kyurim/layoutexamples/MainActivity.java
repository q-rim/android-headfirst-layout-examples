package com.example.kyurim.layoutexamples;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    protected final static String TAG = "---MainActivity:";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickFrameLayout(View view) {
        Log.i(TAG, "onClickFrameLayout");
        Intent intent = new Intent(this, FrameLayoutActivity.class);
        startActivity(intent);
    }

    public void onClickActivity2(View view) {
        Log.i(TAG, "onClickActivity2");
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

    public void onClickActivity3(View view) {
        Log.i(TAG, "onClickActivity3");
        Intent intent = new Intent(this, Activity3.class);
        startActivity(intent);
    }

    public void onClickActivity4(View view) {
        Log.i(TAG, "onClickActivity4");
        Intent intent = new Intent(this, Activity4.class);
        startActivity(intent);
    }
}
