package com.example.kyurim.layoutexamples;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class Activity4 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);

        Toast.makeText(this, "Starting Activity4: <ConstraintLayout>", Toast.LENGTH_SHORT).show();
    }
}
